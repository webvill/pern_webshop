import express from "express";
import expressAsyncHandler from "express-async-handler";
import db from "../db/index.js";
import { isAuth, isAdmin } from "../utils.js";

const orderRouter = express.Router();

orderRouter.get(
   "/mine",
   isAuth,
   expressAsyncHandler(async (req, res) => {
      // { user: req.user.id }
      console.log("req", req);
      const userId = parseInt(req.user.id);
      const orders = await db.query("select * from orders where user_id = $1", [
         userId,
      ]);
      res.send(orders.rows);
   })
);
orderRouter.get(
   "/",
   isAuth,
   isAdmin,
   expressAsyncHandler(async (req, res) => {
      const results = await db.query(
         "select orders.id, created_at, is_paid, paid_at, is_delivered, delivered_at, total_price, name from orders join users on orders.user_id = users.id group by orders.id, users.id"
      );
      res.send(results.rows);
   })
);
orderRouter.post("/", isAuth, async (req, res) => {
   try {
      const { rows } = await db.query(
         "INSERT INTO orders (user_id, payment_method, items_price, shipping_price, tax_price, total_price, is_paid, paid_at, is_delivered) values ($1, $2, $3, $4, $5, $6, $7, $8, $9) returning *",
         [
            req.user.id,
            req.body.paymentMethod,
            req.body.itemsPrice,
            req.body.shippingPrice,
            req.body.taxPrice,
            req.body.totalPrice,
            false,
            new Date().toLocaleDateString(),
            false,
         ]
      );
      const orderId = parseInt(rows[0].id);
      await req.body.orderItems.forEach((item) => {
         const itemId = parseInt(item.id);
         db.query(
            "INSERT INTO order_items (order_id, product_id, slug, name, image, price, quantity) values ($1, $2, $3, $4, $5, $6, $7) returning *",
            [
               orderId,
               itemId,
               item.slug,
               item.name,
               item.image,
               item.price,
               item.quantity,
            ]
         );
      });

      await db.query(
         "INSERT INTO shipping_addresses (order_id, full_name, address, city, postal_code, country) values($1, $2, $3, $4, $5, $6) returning *",
         [
            orderId,
            req.body.shippingAddress.fullName,
            req.body.shippingAddress.address,
            req.body.shippingAddress.city,
            req.body.shippingAddress.postalCode,
            req.body.shippingAddress.country,
         ]
      );
      res.status(201).json({ status: "success", orderId });
   } catch (error) {
      console.log(error);
   }
});
orderRouter.put(
   "/:id/deliver",
   isAuth,
   expressAsyncHandler(async (req, res) => {
      const deliveredAt = new Date().toLocaleDateString();
      if (
         await db.query(
            `update orders set delivered_at = '${deliveredAt}', is_delivered = true where id = $1`,
            [req.params.id]
         )
      ) {
         res.send({ delivered_at: deliveredAt });
      } else {
         res.status(404).send({ message: "Order Not Found" });
      }
   })
);
orderRouter.get("/summary", async (req, res) => {
   res.status(200).json({
      data: {
         users: [{ name: "pelle", numUsers: 19 }],
         orders: [{ name: "pelle", numOrders: 32, totalSales: 890000 }],
         dailyOrders: [
            { id: "2022-04-01", sales: 22 },
            { id: "2022-04-02", sales: 29 },
         ],
         productCategories: [
            { id: "PANTS", count: 22 },
            { id: "SHIRTS", count: 50 },
         ],
      },
   });
});
orderRouter.get("/:id", async (req, res) => {
   try {
      const orderId = parseInt(req.params.id);
      const results = await db.query(
         `select 
            orders.id,
            payment_method, 
            items_price, 
            shipping_price, 
            tax_price, 
            total_price,
            is_paid,
            paid_at,
            is_delivered,
            delivered_at
            full_name,
            address,
            city,
            postal_code,
            country
         from orders 
         
         left join shipping_addresses on orders.id = shipping_addresses.order_id 
         where orders.id = $1
         `,
         [req.params.id]
      );

      const orderItems = await db.query(
         `select 
            order_id,
            product_id,
            slug,
            name,
            image,
            price,
            quantity
         from order_items 
         where order_id = $1`,
         [req.params.id]
      );

      res.status(200).json({
         status: "success",
         results: results.rows.length,
         data: { order: results.rows[0], items: orderItems.rows },
      });
   } catch (error) {
      res.status(404).json({
         message: "Order not found",
      });
   }
});
orderRouter.delete(
   "/:id",
   isAuth,
   isAdmin,
   expressAsyncHandler(async (req, res) => {
      console.log(req.params.id);
      try {
         const order = await db.query("delete from orders where id = $1", [
            parseInt(req.params.id),
         ]);
         res.send({ message: "Order Deleted" });
      } catch (error) {
         console.log("error", error);
         res.status(404).send({ message: "Order Not Found" });
      }
   })
);

export default orderRouter;
