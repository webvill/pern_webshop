import express from "express";
import expressAsyncHandler from "express-async-handler";
import bcrypt from "bcryptjs";
import db from "../db/index.js";
import { isAuth, isAdmin, generateToken } from "../utils.js";

const userRouter = express.Router();

userRouter.put(
   "/profile",
   isAuth,
   expressAsyncHandler(async (req, res) => {
      // { user: req.user.id }
      try {
         const password = bcrypt.hashSync(req.body.password);
         const userId = parseInt(req.user.id);

         const user = await db.query(
            "update users set name=$1, email=$2, password=$3 where id = $4 returning *",
            [req.body.name, req.body.email, password, userId]
         );
         const profile = user.rows[0];
         res.send({
            id: profile.id,
            name: profile.name,
            email: profile.email,
            token: generateToken(profile),
         });
      } catch (error) {
         res.send(404).send({ message: "User not found!" });
      }
   })
);
userRouter.get(
   "/",
   isAuth,
   isAdmin,
   expressAsyncHandler(async (req, res) => {
      console.log("getting users");
      try {
         const users = await db.query("select * from users");
         res.send(users.rows);
      } catch (error) {
         console.log(error);
      }
   })
);
export default userRouter;
