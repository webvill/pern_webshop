import express from "express";
import fs from "fs";
import path from "path";
import expressAsyncHandler from "express-async-handler";
import multer from "multer";
import db from "../db/index.js";
import { isAuth, isAdmin, generateToken } from "../utils.js";

const uploadRouter = express.Router();
const upload = multer({
   dest: "../frontend/public/images",
});
const _dirname = path.resolve();
uploadRouter.post(
   "/",
   isAuth,
   isAdmin,
   upload.single("file"),
   async (req, res) => {
      const tempPath = req.file.path;
      const targetPath =
         "../frontend/public/images/" + `${req.file.originalname}`;
      if (fs.existsSync(targetPath)) {
         res.json({
            data: { secure_url: `/images/${req.file.originalname}` },
         });
         return;
      }
      fs.rename(tempPath, targetPath, (err) => {
         if (err) {
            res.json(err);
            return;
         }
         res.json({
            data: { secure_url: `/images/${req.file.originalname}` },
         });
      });
   }
);

export default uploadRouter;
