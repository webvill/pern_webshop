import express from "express";
import expressAsyncHandler from "express-async-handler";
import db from "../db/index.js";
import { isAuth, isAdmin } from "../utils.js";

const productRouter = express.Router();
productRouter.get("/", async (req, res) => {
   try {
      const results = await db.query(`select 
         products.id,
         products.name as name, 
         products.slug, 
         categories.name as category, 
         image, 
         price,
         countinstock, 
         brands.name as brand, 
         description,
         numreviews,
         rating
      from products
         join categories on products.category_id = categories.id
         join brands on products.brand_id = brands.id
         left join(
            select product_id,
                  count(*) as numreviews,
                  trunc(avg(rating), 1) as rating
            from reviews
            group by product_id
            ) reviews on products.id = reviews.product_id`);

      res.status(200).json({
         status: "success",
         results: results.rows.length,
         data: results.rows,
      });
   } catch (error) {
      console.log(error);
   }
});
productRouter.get(
   "/admin",
   isAuth,
   isAdmin,
   expressAsyncHandler(async (req, res) => {
      const { query } = req;
      const page = query.page || 1;
      const pageSize = query.pageSize || 3;

      const count = await db.query("select count(*) as total from products");
      const products = await db.query(
         `select products.id, products.name as product, price, categories.name as category, brands.name as brand  from products  join categories on categories.id = products.category_id join brands on brands.id = products.brand_id offset ${
            pageSize * (page - 1)
         } limit ${pageSize}`
      );

      res.send({
         products: products.rows,
         countProducts: count.rows[0].total,
         page,
         pages: Math.ceil(count.rows[0].total / pageSize),
      });
   })
);
productRouter.get("/slug/:slug", async (req, res) => {
   try {
      const product = await db.query(
         `select category_id, brand_id, name, price, countinstock, description, products.id, image, images.id as image_id, path from products 
            left join images on images.product_id = products.id where slug = $1`,
         [req.params.slug]
      );

      const id = product.rows[0].id;
      const reviews = await db.query(
         `select name, review, reviews.created_at, trunc(avg(rating), 1) as rating from reviews 
         join users on reviews.user_id = users.id 
         where product_id = $1 
         group by reviews.review, reviews.created_at, reviews.rating, users.name`,
         [id]
      );

      /*  const results = await db.query(
         `select * from products left join(
            select
                  product_id,
                  user_id,
                  review,
                  reviews.created_at,
                  trunc(avg(rating), 1) as rating
                  from reviews
                  join users on reviews.user_id = users.id       
                  group by reviews.product_id, reviews.user_id, review, reviews.created_at
         ) reviews on products.id = reviews.product_id 
            
            left join images on images.product_id = products.id 
            where slug = $1 group by products.id, images.id, reviews.product_id, reviews.user_id, reviews.review, reviews.created_at, reviews.rating`,
         [req.params.slug]
      );
      
      res.status(200).json({
         status: "success",
         product: results.rows[0],
         reviews: results.rows,
      }); */
      console.log("results", reviews.rows);
      res.status(200).json({
         status: "success",
         images: product.rows.map((row) => row.path),
         product: product.rows[0],
         reviews: reviews.rows,
      });
   } catch (error) {
      res.status(404).json({
         message: `Product with slug ${req.params.slug} not found due to ${error}`,
      });
   }
});

productRouter.get(
   "/categories",
   expressAsyncHandler(async (req, res) => {
      // { user: req.user.id }

      const results = await db.query("select * from categories");

      res.send(results.rows);
   })
);
productRouter.get(
   "/brands",
   expressAsyncHandler(async (req, res) => {
      // { user: req.user.id }

      const results = await db.query("select * from brands");

      res.send(results.rows);
   })
);
productRouter.get(
   "/search",

   expressAsyncHandler(async (req, res) => {
      const page = parseInt(req.query.page);
      const limit = 6;
      const offset = limit * (req.query.page - 1);

      let where = false;
      let queryBuilder = "";
      const constraints = Object.entries(req.query).filter(
         (c) => c[0] !== "order" && c[0] !== "page"
      );
      const priceRange = req.query.price.split("-");
      console.log(priceRange);
      constraints.forEach((q) => {
         if (q[1] !== "" && q[1] !== "all") {
            const pre = where ? "and " : "where ";

            if (q[0] === "price") {
               if (priceRange.length > 0) {
                  queryBuilder +=
                     pre +
                     `price >= ${priceRange[0]} and price <= ${priceRange[1]} `;
                  where = true;
               }
            }
            if (q[0] === "rating") {
               queryBuilder += pre + `reviews.rating >= ${q[1]} `;
               where = true;
            }
            if (q[0] === "category") {
               queryBuilder += pre + `categories.name = '${q[1]}' `;
               where = true;
            }
            if (q[0] === "query") {
               queryBuilder +=
                  pre + `LOWER(products.name) like LOWER('%${q[1]}%') `;
               where = true;
            }
            if (q[0] === "brand") {
               queryBuilder +=
                  pre + `LOWER(brands.name) like LOWER('%${q[1]}%') `;
               where = true;
            }
         }
      });
      let orderBy;
      switch (req.query.order) {
         case "highest":
            orderBy = "price DESC";
            break;
         case "lowest":
            orderBy = "price ASC";
            break;
         case "toprated":
            orderBy = "reviews.rating DESC";
            break;
         case "newest":
            orderBy = "date ASC";
            break;
         default:
            break;
      }
      console.log("query", queryBuilder);
      const count = await db.query(
         "select count(*) as productscount from products"
      );
      const results = await db.query(`select 
      products.id,
         products.name as name, 
         products.slug, 
         categories.name as category, 
         image, 
         price,
         countinstock, 
         brands.name as brand, 
         description,
         numreviews,
         rating
         from products
         join categories on products.category_id = categories.id
         join brands on products.brand_id = brands.id
         left join(
            select product_id,
                  count(*) as numreviews,
                  trunc(avg(rating), 1) as rating

            from reviews
            group by product_id
            ) reviews on products.id = reviews.product_id
 ${
    queryBuilder !== "" ? queryBuilder : ""
 } ORDER BY ${orderBy} limit ${limit} offset ${offset}`);
      res.send({
         products: results.rows,
         page: page,
         pages: Math.ceil(count.rows[0].productscount / limit),
         countProducts: results.rows.length,
      });
   })
);
productRouter.get("/:id", async (req, res) => {
   try {
      const images = await db.query(
         `select * from images where product_id = $1`,
         [req.params.id]
      );
      const results = await db.query(`select * from products where id = $1`, [
         req.params.id,
      ]);
      res.status(200).json({
         status: "success",
         results: results.rows.length,
         data: { product: results.rows[0], images: images.rows },
      });
   } catch (error) {
      res.status(404).json({
         message: "Product not found",
      });
   }
});

productRouter.post(
   "/",
   isAuth,
   isAdmin,
   expressAsyncHandler(async (req, res) => {
      const products = await db
         .query(
            "insert into products (category_id, brand_id, name, slug, image, price, countinstock, description, date) values ($1, $2, $3, $4, $5, $6, $7, $8, $9) returning *",
            [
               req.body.category_id,
               req.body.brand_id,
               req.body.name,
               req.body.slug,
               req.body.image,
               req.body.price,
               req.body.countinstock,
               req.body.description,
               new Date(),
            ]
         )
         .then((products) => {
            if (req.body.images.length > 0) {
               const productId = products.rows[0].id;
               const images = [];
               for (const img of req.body.images) {
                  images.push(`(${productId}, '${img.path}')`);
               }
               db.query(
                  `insert into images (product_id, path) values ${images.split(
                     ","
                  )}`
               );
               console.log("product", products);
            }
            res.send({ message: "Product Created" });
         })
         .catch((error) => console.log(error));
   })
);
productRouter.put(
   "/:id",
   isAuth,
   isAdmin,
   expressAsyncHandler(async (req, res) => {
      const productId = parseInt(req.params.id);
      console.log("bodyreq", req.body);
      console.log("id", productId);
      const products = await db
         .query(
            `update products 
               set category_id = $1, 
               brand_id = $2, 
               name = $3, 
               slug = $4, 
               image = $5, 
               price = $6, 
               countinstock = $7, 
               description = $8, 
               date = $9
               where id = ${productId} 
               returning *`,
            [
               req.body.category_id,
               req.body.brand_id,
               req.body.name,
               req.body.slug,
               req.body.image,
               req.body.price,
               req.body.countinstock,
               req.body.description,
               new Date(),
            ]
         )
         .then((products) => {
            console.log("images", req.body.images);
            if (req.body.images.length > 0) {
               const images = [];
               for (const img of req.body.images) {
                  images.push(`(${productId}, '${img}')`);
               }
               db.query(`delete from images where product_id = ${productId}`);
               db.query(
                  `insert into images (product_id, path) values ${images.join(
                     ","
                  )}`
               );
               console.log("product", products);
            }
            res.send({ message: "Product Created" });
         })
         .catch((error) => console.log(error));
   })
);
productRouter.delete(
   "/:id",
   isAuth,
   isAdmin,
   expressAsyncHandler(async (req, res) => {
      if (
         await db.query("delete from products where id = $id", req.params.id)
      ) {
         res.send({ message: "Product Deleted" });
      } else {
         res.status(404).send({ message: "Product Not Found" });
      }
   })
);
productRouter.post(
   "/:id/reviews",
   isAuth,
   expressAsyncHandler(async (req, res) => {
      console.log(req.params.id);
      const productId = parseInt(req.params.id);
      const review = await db.query(
         `with ins as ( 
            insert into reviews (product_id, review, rating, user_id, created_at) 
            values ($1, $2, $3, $4, NOW())  
            on conflict (user_id, product_id) do update set review = $2, rating = $3 where reviews.product_id = $1 returning * 
            ) 
            select ins.user_id, ins.review, ins.rating, ins.created_at, users.name 
            from ins 
            join users on users.id = ins.user_id`,
         [productId, req.body.review, req.body.rating, req.user.id]
      );

      res.status(201).send({
         message: "Review Created",
         review: review.rows[0],
      });
      /* if (product) {
         res.status(201).send({ message: "lkjs" });
      } else {
         res.status(404).send({ message: "Product Not Found" });
      } */
   })
);
export default productRouter;
