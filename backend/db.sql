CREATE TABLE categories (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE brands (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE products (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    category_id BIGINT NOT NULL REFERENCES categories(id),
    brand_id BIGINT NULL REFERENCES brands(id),
    name VARCHAR(50) NOT NULL,
    slug VARCHAR(50) NOT NULL,
    image VARCHAR(50) NOT NULL,
    price INT NOT NULL,
    countInStock INT NOT NULL,
    description TEXT NULL
);

CREATE TABLE reviews (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    product_id BIGINT NOT NULL REFERENCES products(id),
    review TEXT NOT NULL,
    rating INT NOT NULL check(rating >= 1 and rating <= 5)
);

CREATE TABLE users (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    isAdmin BOOLEAN NOT NULL
);
CREATE TABLE orders (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    user_id BIGINT NOT NULL REFERENCES users(id),
    payment_method INT NOT NULL,
    items_price INT NOT NULL,
    shipping_price INT NOT NULL,
    tax_price INT NOT NULL,
    total_price INT NOT NULL,
    is_paid BOOLEAN DEFAULT(false),
    paid_at DATE,
    is_delivered BOOLEAN DEFAULT(false),
    delivered_at DATE
);

CREATE TABLE order_items (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    order_id BIGINT NULL REFERENCES orders(id),
    product_id BIGINT NULL REFERENCES orders(id),
    slug VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    image VARCHAR(255) NOT NULL,
    price INT NOT NULL,
    quantity INT NOT NULL
);
CREATE TABLE shipping_addresses (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    order_id BIGINT NULL REFERENCES orders(id),
    full_name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    city VARCHAR(255) NOT NULL,
    postal_code VARCHAR(255) NOT NULL,
    country VARCHAR(255) NOT NULL
);
CREATE TABLE images (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    product_id BIGINT NOT NULL REFERENCES products(id),
    path VARCHAR(100) NOT NULL
);

select *
from restaurants
    left join(
        select restaurant_id,
            count(*),
            trunc(avg(rating), 1) as average_rating
        from reviews
        group by restaurant_id
    ) reviews on restaurants.id = reviews.restaurant_id;

select 
    products.name as name, 
    products.slug, 
    categories.name as category, 
    image, price, 
    brands.name as brand, 
    description,
    numReviews,
    average_rating
from products
    join categories on products.category_id = categories.id
    join brands on products.brand_id = brands.id
    left join(
        select product_id,
            count(*) as numReviews,
            trunc(avg(rating), 1) as average_rating
        from reviews
        group by product_id
    ) reviews on products.id = reviews.product_id;

SELECT restaurant_id, TRUNC(AVG(rating), 2) as avg_rating, COUNT(rating) as total_reviews FROM reviews GROUP BY restaurant_id

INSERT INTO brands (name, slug) VALUES('NIKE', 'nike');
INSERT INTO brands (name, slug) VALUES('Adidas', 'adidas');
INSERT INTO brands (name, slug) VALUES('Puma', 'puma');
INSERT INTO categories (name, slug) VALUES('Shirts', 'shirts');
INSERT INTO categories (name, slug) VALUES('Pants', 'pants');

INSERT INTO products (category_id,brand_id, name,slug,image, price,countInStock,description) VALUES(4, 1,'Nike Slim shirt', 'nike-slim-shirt', '/images/p1.jpg', 120, 10, 'high quality shirt');
INSERT INTO products (category_id,brand_id, name,slug,image, price,countInStock,description) VALUES(4, 2,'Adidas Fit Shirt', 'adidas-fit-shirt', '/images/p2.jpg', 250, 20, 'high quality shirt');
INSERT INTO products (category_id,brand_id,name,slug,image, price,countInStock,description) VALUES(5, 1,'Nike Slim Pant', 'nike-slim-pant', '/images/p3.jpg', 25, 0, 'high quality shirt');
INSERT INTO products (category_id,brand_id,name,slug,image, price,countInStock,description) VALUES(5, 2,'Adidas Fit Pant', 'adidas-fit-pant', '/images/p4.jpg', 65, 5, 'high quality shirt');

INSERT INTO reviews (product_id, user_id, review, rating, created_at) VALUES(13, 3, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4, NOW());
INSERT INTO reviews (product_id, review, rating) VALUES(13, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 5);
INSERT INTO reviews (product_id, review, rating) VALUES(13, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 5);
INSERT INTO reviews (product_id, review, rating) VALUES(13, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(13, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(13, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(13, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 5);
INSERT INTO reviews (product_id, review, rating) VALUES(13, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);

INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);
INSERT INTO reviews (product_id, review, rating) VALUES(14, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 3);

INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(15, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);

INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);
INSERT INTO reviews (product_id, review, rating) VALUES(16, 'Architecto quasi qui iure. Architecto fugiat assumenda et nihil aspernatur libero quisquam. Quas magnam eveniet et consequatur.', 4);


INSERT INTO restaurants (name, location, price_range) VALUES('Red rum', 'Stockholm', 3);
INSERT INTO restaurants (name, location, price_range) VALUES('Red rum', 'Stockholm', 3);
INSERT INTO restaurants (name, location, price_range) VALUES('Red rum', 'Stockholm', 3);
INSERT INTO reviews (restaurant_id, name, review, rating) VALUES(3, 'Danny', 'safakfd askdfj wkaej rlkja fak', 3);
