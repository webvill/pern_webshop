//import {} from "dotenv/config";
import express from "express";
import bcrypt from "bcryptjs";
import path from "path";
import db from "./db/index.js";
import { generateToken, isAuth } from "./utils.js";
import cors from "cors";
import orderRouter from "./routes/orderRoutes.js";
import userRouter from "./routes/userRoutes.js";
import productRouter from "./routes/productRoutes.js";
import uploadRouter from "./routes/uploadRoutes.js";
const app = express();
app.use(express.json());
app.use(cors());

app.use("/api/orders", orderRouter);
app.use("/api/users", userRouter);
app.use("/api/products", productRouter);
app.use("/api/upload", uploadRouter);
if (process.env.ENV === "production") {
   const _dirname = path.resolve();
   app.use(express.static(path.join(_dirname, "/frontend/build")));
   app.get("*", (req, res) => {
      res.sendFile(path.join(_dirname, "/frontend/build/index.html"));
   });
}

app.post("/api/users/register", async (req, res) => {
   // insert user
   const password = bcrypt.hashSync(req.body.password);
   const isAdmin = true;
   try {
      const { rows } = await db.query(
         "INSERT INTO users (name, email, password, isadmin) values ($1, $2, $3, $4) returning *",
         [req.body.name, req.body.email, password, isAdmin]
      );

      res.status(201).json({ status: "success", data: rows[0] });
   } catch (error) {
      console.log(error);
   }
});
app.post("/api/users/signin", async (req, res) => {
   // find user with credentials
   const { rows } = await db.query(
      `select id, name, email, password, isadmin from users where email = $1`,
      [req.body.email]
   );
   if (rows.length > 0) {
      const user = rows[0];
      if (bcrypt.compareSync(req.body.password, user.password)) {
         res.status(201).json({
            status: "success",
            data: {
               id: user.id,
               name: user.name,
               email: user.email,
               isadmin: user.isadmin,
               token: generateToken(user),
            },
         });
      }
      return;
   }
   res.status(401).json({ message: "Invalid email or password" });
});

app.get("/api/keys/paypal", async (req, res) => {
   res.status(200).json({ status: "success", data: "clientIdString" });
});
app.put("/api/orders/:id/pay", async (req, res) => {
   try {
      const { rows } = await db.query(
         "update orders set is_paid = true where id = $1",
         [req.params.id]
      );
      res.status(200).json({ status: "success", data: true });
   } catch (error) {
      res.status(404).json({
         message: "Order not found",
      });
   }
});
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server running at ${port}`));
