export default function App() {
   return (
      <Provider>
         <AddTodo />
         <TodoList />
      </Provider>
   );
}
