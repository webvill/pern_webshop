// /src/app/store.js
import { configureStore } from "@reduxjs/toolkit";
import usersReducer from "../features/users/usersSlice";
import authReducer from "../features/users/authSlice";
import postsReducer from "../features/posts/postsSlice";
//import commentsReducer from "../features/comments/commentsSlice";

export default configureStore({
   reducer: {
      posts: postsReducer,
      users: usersReducer,
      auth: authReducer,
      //comments: commentsReducer,
   },
});
