import { useDispatch, useSelector } from "react-redux";

import { addPost, updatePost } from "./productsThunk";
import React, { useState } from "react";

import { useNavigate } from "react-router-dom";

const PostForm = ({ post }) => {
   const {id} = useSelector(state => state.auth.userData)
   const [title, setTitle] = useState(post ? post.title : "");
   const [content, setContent] = useState(post ? post.content : "");
   const [userId, setUserId] = useState(post ? post.userId : id);
   const navigate = useNavigate();
   const dispatch = useDispatch();
   //const users = useSelector((state) => state.users);

   const onTitleChanged = (e) => setTitle(e.target.value);
   const onContentChanged = (e) => setContent(e.target.value);
   //const onAuthorChanged = (e) => setUserId(e.target.value);

   const submit = () => {
      if (title && content) {
         if (post) {
            dispatch(updatePost({ title, content, id: post.id }));
            navigate(`/posts/${post.id}`);
         } else {
            dispatch(addPost({ title, content}));
         }

         setTitle("");
         setContent("");
         setUserId("");
      }
   };
   const canSave = Boolean(title) && Boolean(content) && Boolean(id === userId);

   /* const usersOptions = users.map((user) => (
      <option key={user.id} value={user.id}>
         {user.name}
      </option>
   )); */

   return (
      <section>
         <h2>{post ? "Uppdatera post" : "Skapa nytt inlägg"}</h2>
         <form>
            <div className="mb-3">
               <label htmlFor="postTitle">Rubrik</label>
               <input
                  className="form-control"
                  type="text"
                  id="postTitle"
                  name="postTitle"
                  value={title}
                  onChange={onTitleChanged}
               />
            </div>
            {/* <div className="mb-3">
               <label htmlFor="postAuthor">Author:</label>
               <select
                  className="form-select"
                  id="postAuthor"
                  value={userId}
                  onChange={onAuthorChanged}
               >
                  <option value=""></option>
                  {usersOptions}
               </select>
            </div> */}
            <div className="mb-3">
               <label htmlFor="postContent">Content</label>
               <textarea
                  className="form-control"
                  id="postContent"
                  name="postContent"
                  value={content}
                  onChange={onContentChanged}
               />
            </div>
            <button
               className="btn btn-outline-primary"
               type="button"
               onClick={submit}
               disabled={!canSave}
            >
               {post ? "Uppdatera" : "Skapa"}
            </button>
         </form>
      </section>
   );
};
export default PostForm;
