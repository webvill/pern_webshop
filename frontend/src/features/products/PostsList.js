import { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { fetchPosts } from "./postsThunk";
import store from "../../app/store";
import { PostAuthor } from "./PostAuthor";
import { TimeAgo } from "./TimeAgo";

const PostsList = () => {
   // Sort posts in reverse chronological order by datetime string
   
   const posts = useSelector((state) => state.posts.posts);
   console.log('posts from plist', posts)
   const orderedPosts = posts
      .slice()
      .sort((a, b) => b.date.localeCompare(a.date));
   const renderedPosts = orderedPosts.map((post) => (
      <article className="post-excerpt" key={post.id}>
         <h3>
            <Link to={`/posts/${post.id}`}>{post.title}</Link>
         </h3>
         <PostAuthor userId={post.userId} author={post.user}/>
         <TimeAgo timestamp={post.date} />
         <p className="post-content">{post.content.substring(0, 100)}</p>
      </article>
   ));
   
      useEffect(() => {
        store.dispatch(fetchPosts());
      
      }, [])
      
   return (
      <section className="posts-list">
         <h2>Posts</h2>
         {renderedPosts}
      </section>
   );
};
export default PostsList;
