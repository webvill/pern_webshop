import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import HomeScreen from "./pages/home";
import ProductScreen from "./pages/product";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Badge from "react-bootstrap/Badge";
import { LinkContainer } from "react-router-bootstrap";
import { Store } from "./Store";
import { useContext, useState, useEffect } from "react";
import CartScreen from "./pages/cart";
import SigninScreen from "./pages/signin";
import RegisterScreen from "./pages/register";
import api from "./services/api";
import ProfileScreen from "./pages/profile";
import ShippingScreen from "./pages/shipping";
import PaymentMethodScreen from "./pages/paymentMethod";
import PlaceOrderScreen from "./pages/placeOrder";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import OrderScreen from "./pages/order";
import OrderHistoryScreen from "./pages/orderHistory";
import ProtectedRoute from "./components/ProtectedRoute";
import DashboardScreen from "./pages/admin/dashboard";
import AdminRoute from "./components/AdminRoute";
import { getError } from "./utils";
import SearchBox from "./components/Searchbox";
import { List } from "react-bootstrap-icons";
import SearchScreen from "./pages/search";
import ProductListScreen from "./pages/admin/productList";
import ProductEditScreen from "./pages/admin/productEdit";
import ProductCreateScreen from "./pages/admin/productCreate";
import OrderListScreen from "./pages/admin/orderList";
import UserListScreen from "./pages/admin/userList";

function App() {
   const [sidebarIsOpen, setSidebarIsOpen] = useState(false);
   const [categories, setCategories] = useState([]);
   const { state, dispatch } = useContext(Store);
   const [resetSearchForm, setResetSearchForm] = useState(true);
   const { cart, user } = state;

   const signoutHandler = (e) => {
      dispatch({ type: "USER_LOGOUT" });
      localStorage.removeItem("userInfo");
      localStorage.removeItem("paymentMethod");
      window.location.href = "/signin";
   };

   useEffect(() => {
      const fetchCategories = async () => {
         const { data } = await api
            .get("/products/categories")
            .then()
            .catch((err) => toast.error(getError(err)));
         setCategories(data);
      };
      fetchCategories();
   }, []);
   return (
      <BrowserRouter>
         <div
            className={
               sidebarIsOpen
                  ? "d-flex flex-column site-container active-container"
                  : "d-flex flex-column site-container"
            }
         >
            <ToastContainer position="bottom-center" limit={1} />
            <header>
               <Navbar variant="dark" className="main-nav" expand={"lg"}>
                  <Container>
                     <List
                        onClick={() => setSidebarIsOpen(!sidebarIsOpen)}
                        className="text-white fs-3 me-3"
                        style={{ cursor: "pointer" }}
                     />
                     <LinkContainer to="/">
                        <Navbar.Brand>shopping central</Navbar.Brand>
                     </LinkContainer>
                     <Navbar.Toggle aria-controls="basic-navbar-nav" />
                     <Navbar.Collapse id="basic-navbar-nav">
                        <SearchBox
                           resetSearchForm={resetSearchForm}
                           setResetSearchForm={setResetSearchForm}
                        />
                        <Nav className="me-auto w-100 justify-content-end">
                           <Link to="/cart" className="nav-link text-white">
                              Cart
                              {cart.cartItems.length > 0 && (
                                 <Badge pill bg="danger">
                                    {cart.cartItems.reduce(
                                       (a, c) => a + c.quantity,
                                       0
                                    )}
                                 </Badge>
                              )}
                           </Link>
                        </Nav>
                        {user.userInfo ? (
                           !user.userInfo.isadmin ? (
                              <NavDropdown
                                 title={user.userInfo.name}
                                 id="basic-nav-dropdown"
                              >
                                 <LinkContainer to="/profile">
                                    <NavDropdown.Item>Profile</NavDropdown.Item>
                                 </LinkContainer>
                                 <LinkContainer to="/orderhistory">
                                    <NavDropdown.Item>
                                       Order history
                                    </NavDropdown.Item>
                                 </LinkContainer>
                                 <NavDropdown.Divider />
                                 <Link
                                    className="dropdown-item"
                                    to="#signout"
                                    onClick={signoutHandler}
                                 >
                                    Logga ut
                                 </Link>
                              </NavDropdown>
                           ) : (
                              <NavDropdown
                                 title="Admin"
                                 id="admin-nav-dropdown"
                              >
                                 <LinkContainer to="/admin/dashboard">
                                    <NavDropdown.Item>
                                       Dashboard
                                    </NavDropdown.Item>
                                 </LinkContainer>
                                 <LinkContainer to="/admin/products">
                                    <NavDropdown.Item>
                                       Products
                                    </NavDropdown.Item>
                                 </LinkContainer>
                                 <LinkContainer to="/admin/orders">
                                    <NavDropdown.Item>Orders</NavDropdown.Item>
                                 </LinkContainer>
                                 <LinkContainer to="/admin/users">
                                    <NavDropdown.Item>Users</NavDropdown.Item>
                                 </LinkContainer>
                                 <NavDropdown.Divider />
                                 <Link
                                    className="dropdown-item"
                                    to="#signout"
                                    onClick={signoutHandler}
                                 >
                                    Logga ut
                                 </Link>
                              </NavDropdown>
                           )
                        ) : (
                           <>
                              <Link to="/signin">Signin</Link>
                           </>
                        )}
                     </Navbar.Collapse>
                  </Container>
               </Navbar>
            </header>
            <div
               className={
                  sidebarIsOpen
                     ? "active-nav side-navbar d-flex justify-content-between flex-wrap flex-column"
                     : "side-navbar d-flex justify-content-between flex-wrap flex-column"
               }
            >
               <Nav className="flex-column text-white w-100 p-2">
                  <Nav.Item>
                     <strong>Categories</strong>
                  </Nav.Item>
                  {categories.map((category) => (
                     <Nav.Item key={category.id}>
                        <LinkContainer
                           to={`/search?category=${category.name}`}
                           onClick={() => setSidebarIsOpen(false)}
                        >
                           <Nav.Link>{category.name}</Nav.Link>
                        </LinkContainer>
                     </Nav.Item>
                  ))}
               </Nav>
            </div>
            <main>
               <Container className="mt-3">
                  <Routes>
                     <Route
                        path="/products/slug/:slug"
                        element={<ProductScreen />}
                     />
                     <Route path="/" element={<HomeScreen />} />
                     <Route path="/cart" element={<CartScreen />} />
                     <Route path="/signin" element={<SigninScreen />} />
                     <Route path="/register" element={<RegisterScreen />} />
                     <Route
                        path="/profile"
                        element={
                           <ProtectedRoute>
                              <ProfileScreen />
                           </ProtectedRoute>
                        }
                     />
                     <Route path="/shipping" element={<ShippingScreen />} />
                     <Route path="/payment" element={<PaymentMethodScreen />} />
                     <Route path="/placeorder" element={<PlaceOrderScreen />} />
                     <Route
                        path="/order/:id"
                        element={
                           <ProtectedRoute>
                              <OrderScreen />
                           </ProtectedRoute>
                        }
                     ></Route>
                     <Route
                        path="/search"
                        element={
                           <SearchScreen
                              setResetSearchForm={setResetSearchForm}
                           />
                        }
                     />
                     <Route
                        path="/orderhistory"
                        element={
                           <ProtectedRoute>
                              <OrderHistoryScreen />
                           </ProtectedRoute>
                        }
                     ></Route>
                     <Route
                        path="/admin/dashboard"
                        element={
                           <AdminRoute>
                              <DashboardScreen />
                           </AdminRoute>
                        }
                     ></Route>
                     <Route
                        path="/admin/products"
                        element={
                           <AdminRoute>
                              <ProductListScreen />
                           </AdminRoute>
                        }
                     ></Route>
                     <Route
                        path="/admin/product/:id/edit"
                        element={
                           <AdminRoute>
                              <ProductEditScreen />
                           </AdminRoute>
                        }
                     ></Route>
                     <Route
                        path="/admin/product/create"
                        element={
                           <AdminRoute>
                              <ProductCreateScreen />
                           </AdminRoute>
                        }
                     ></Route>
                     <Route
                        path="/admin/orders"
                        element={
                           <AdminRoute>
                              <OrderListScreen />
                           </AdminRoute>
                        }
                     ></Route>
                     <Route
                        path="/admin/users"
                        element={
                           <AdminRoute>
                              <UserListScreen />
                           </AdminRoute>
                        }
                     ></Route>
                  </Routes>
               </Container>
            </main>
            <footer>
               <div className="text-center">all rights reserverd</div>
            </footer>
         </div>
      </BrowserRouter>
   );
}

export default App;
