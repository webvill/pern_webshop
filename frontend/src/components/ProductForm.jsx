import React, { useEffect, useState, useReducer } from "react";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import Form from "react-bootstrap/Form";
import { toast } from "react-toastify";
import api from "../services/api";
import { getError } from "../utils";
import LoadingBox from "./LoadingBox";
import MessageBox from "./MessageBox";
import { XCircleFill } from "react-bootstrap-icons";

const reducer = (state, action) => {
   switch (action.type) {
      case "UPLOAD_REQUEST":
         return { ...state, loadingUpload: true, errorUpload: "" };
      case "UPLOAD_SUCCESS":
         return {
            ...state,
            loadingUpload: false,
            errorUpload: "",
         };
      case "UPLOAD_FAIL":
         return { ...state, loadingUpload: false, errorUpload: action.payload };

      default:
         return state;
   }
};

const ProductForm = ({ submitHandler, user, product }) => {
   const [name, setName] = useState(product ? product.name : "");
   const [slug, setSlug] = useState(product ? product.slug : "");
   const [price, setPrice] = useState(product ? product.price : "");
   const [image, setImage] = useState(product ? product.image : "");
   const [images, setImages] = useState(product ? product.images : []);
   const [category, setCategory] = useState(product ? product.category_id : "");
   const [countInStock, setCountInStock] = useState(
      product ? product.countinstock : ""
   );
   const [brand, setBrand] = useState(product ? product.brand_id : "");
   const [description, setDescription] = useState(
      product ? product.description : ""
   );
   const [brands, setBrands] = useState([]);
   const [categories, setCategories] = useState([]);

   const [{ loadingUpdate, loadingUpload }, dispatch] = useReducer(reducer, {
      loading: true,
      error: "",
   });
   // name, slug, price, image, images, category, countInStock, brand, descriptions
   useEffect(() => {
      const fetchBrands = async () => {
         await api
            .get("/products/brands")
            .then(({ data }) => setBrands(data))
            .catch((err) => console.log(err));
      };

      fetchBrands();
   }, []);
   useEffect(() => {
      const fetchCategories = async () => {
         await api
            .get("/products/categories")
            .then(({ data }) => setCategories(data))
            .catch((err) => console.log(err));
      };

      fetchCategories();
   }, []);

   const uploadFileHandler = async (e, forImages) => {
      const file = e.target.files[0];
      const bodyFormData = new FormData();
      bodyFormData.append("file", file);
      try {
         dispatch({ type: "UPLOAD_REQUEST" });
         e.preventDefault();
         const {
            data: { data },
         } = await api.post("/upload", bodyFormData, {
            headers: {
               "Content-Type": "multipart/form-data",
               authorization: `Bearer ${user.token}`,
            },
         });
         dispatch({ type: "UPLOAD_SUCCESS" });

         if (forImages) {
            setImages([...images, data.secure_url]);
         } else {
            console.log("data ", data);
            setImage(data.secure_url);
         }
         toast.success("Image uploaded successfully. click Update to apply it");
      } catch (err) {
         toast.error(getError(err));
         dispatch({ type: "UPLOAD_FAIL", payload: getError(err) });
      }
   };
   const deleteFileHandler = async (fileName, f) => {
      console.log(fileName, f);
      setImages(images.filter((x) => x !== fileName));
      toast.success("Image removed successfully. click Update to apply it");
   };
   return (
      <Form
         onSubmit={(e) =>
            submitHandler(e, {
               name,
               slug,
               price,
               image,
               images,
               category,
               countInStock,
               brand,
               description,
            })
         }
      >
         <Form.Group className="mb-3" controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control
               value={name}
               onChange={(e) => setName(e.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="slug">
            <Form.Label>Slug</Form.Label>
            <Form.Control
               value={slug}
               onChange={(e) => setSlug(e.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="name">
            <Form.Label>Price</Form.Label>
            <Form.Control
               value={price}
               onChange={(e) => setPrice(e.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="image">
            <Form.Label>Image File</Form.Label>
            <Form.Control
               value={image}
               onChange={(e) => setImage(e.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="imageFile">
            <Form.Label>Upload Image</Form.Label>
            <Form.Control type="file" onChange={uploadFileHandler} />
            {loadingUpload && <LoadingBox></LoadingBox>}
         </Form.Group>

         <Form.Group className="mb-3" controlId="additionalImage">
            <Form.Label>Additional Images</Form.Label>
            {images.length === 0 && <MessageBox>No image</MessageBox>}
            <ListGroup variant="flush">
               {images.map((image, idx) => (
                  <ListGroup.Item key={idx}>
                     <img
                        src={image.path}
                        width="100"
                        alt=""
                        className="me-3"
                     />
                     {image.path}
                     <XCircleFill
                        className="mx-3"
                        onClick={() => deleteFileHandler(image)}
                     />
                  </ListGroup.Item>
               ))}
            </ListGroup>
         </Form.Group>
         <Form.Group className="mb-3" controlId="additionalImageFile">
            <Form.Label>Upload Aditional Image</Form.Label>
            <Form.Control
               type="file"
               onChange={(e) => uploadFileHandler(e, true)}
            />
            {loadingUpload && <LoadingBox></LoadingBox>}
         </Form.Group>

         <Form.Select
            className="mb-3"
            controlId="category"
            value={category}
            onChange={(e) => setCategory(e.target.value)}
            required
         >
            {!product && <option>Open this select menu</option>}
            {categories.map(({ id, name }) => (
               <option key={id} value={id}>
                  {name}
               </option>
            ))}
         </Form.Select>
         <Form.Select
            className="mb-3"
            controlId="brand"
            value={brand}
            onChange={(e) => setBrand(e.target.value)}
            required
         >
            {!product && <option>Open this select menu</option>}
            {brands.map(({ id, name }) => (
               <option key={id} value={id}>
                  {name}
               </option>
            ))}
         </Form.Select>

         <Form.Group className="mb-3" controlId="countInStock">
            <Form.Label>Count In Stock</Form.Label>
            <Form.Control
               value={countInStock}
               onChange={(e) => setCountInStock(e.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
               value={description}
               onChange={(e) => setDescription(e.target.value)}
               required
            />
         </Form.Group>
         <div className="mb-3">
            <Button disabled={loadingUpdate} type="submit">
               {product ? "Update" : "Create"}
            </Button>
            {loadingUpdate && <LoadingBox></LoadingBox>}
         </div>
      </Form>
   );
};

export default ProductForm;
