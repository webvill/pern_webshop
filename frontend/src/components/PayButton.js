import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
const PayButton = ({ createOrder, onApprove, onError }) => {
   console.log("createOrder", createOrder);
   console.log("onApprove", onApprove);
   console.log("onError", onError);
   const [show, setShow] = useState(false);
   const [orderValue, setOrderValue] = useState(0);

   const actions = {
      order: {
         create: (payload) => {
            return new Promise((resolve, reject) => {
               const orderId = 1;
               console.log("params", payload.purchase_units[0].amount.value);
               if (true) {
                  setOrderValue(payload.purchase_units[0].amount.value);

                  resolve(() => {
                     return orderId;
                  });
               } else {
                  reject("FAILURE");
               }
            });
         },
         capture: (payload) => {
            return new Promise((resolve, reject) => {
               console.log("params", payload);
               if (true) {
                  resolve();
               } else {
                  reject("FAILURE");
               }
            });
         },
      },
   };

   const onClickHandler = () => {
      createOrder(actions);
      setShow(true);
   };
   return (
      <>
         <button onClick={onClickHandler} className="btn btn-outline-success">
            PayButton
         </button>
         <Modal show={show} onHide={() => setShow(false)}>
            <Modal.Header closeButton>
               <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               Value: {orderValue}{" "}
               <button
                  className="btn btn-outline-success"
                  onClick={() => onApprove(actions)}
               >
                  approve
               </button>
            </Modal.Body>
         </Modal>
      </>
   );
};

export default PayButton;
