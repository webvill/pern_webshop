import React, { useState, useEffect } from "react";
import FormControl from "react-bootstrap/FormControl";
import Form from "react-bootstrap/Form";
import { useNavigate } from "react-router-dom";
import { Search } from "react-bootstrap-icons";

export default function SearchBox({ resetSearchForm, setResetSearchForm }) {
   const navigate = useNavigate();
   const [query, setQuery] = useState("");
   const submitHandler = (e) => {
      e.preventDefault();
      setResetSearchForm(false);
      navigate(query ? `/search/?query=${query}` : "/search");
   };
   useEffect(() => {
      if (resetSearchForm) setQuery("");
   }, [resetSearchForm]);
   return (
      <Form className="d-flex me-auto" onSubmit={submitHandler}>
         <div
            className="bg-white d-flex justify-content-between align-items-center pe-3"
            style={{ width: "300px" }}
         >
            <FormControl
               type="text"
               value={query}
               name="q"
               id="q"
               onChange={(e) => setQuery(e.target.value)}
               placeholder="search products..."
               aria-label="Search Products"
               style={{ border: "none", boxShadow: "none" }}
            />
            <Search />
         </div>
      </Form>
   );
}
