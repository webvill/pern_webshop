import { StarFill, StarHalf, Star } from "react-bootstrap-icons";
const Rating = ({ rating, numReviews }) => {
   const stars = [];
   for (let i = 1; i < 6; i++) {
      if (rating >= i) stars.push(<StarFill key={i} color="gold" />);
      else if (rating >= i - 0.5) stars.push(<StarHalf key={i} color="gold" />);
      else stars.push(<Star key={i} color="gold" />);
   }
   return (
      <div className="rating">
         <span>{stars}</span> {numReviews && <span>({numReviews})</span>}
      </div>
   );
};

export default Rating;
