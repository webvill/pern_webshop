import { useContext } from "react";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Rating from "./Rating";
import { Store } from "../Store";

const Product = ({ product }) => {
   const { state, dispatch } = useContext(Store);
   const { cart } = state;
   const addToCartHandler = (product) => {
      dispatch({
         type: "CART_ADD_ITEM",
         payload: { ...product, quantity: 1 },
      });
   };
   return (
      <Card>
         <Link to={`/products/slug/${product.slug}`}>
            <img
               src={product.image}
               alt={product.name}
               className="card-img-top"
            />
         </Link>
         <Card.Body>
            <Link to={`/products/slug/${product.slug}`}>
               <Card.Title>{product.name}</Card.Title>
            </Link>
            <Rating rating={product.rating} numReviews={product.numreviews} />
            <Card.Text>
               <strong> S{product.price}</strong>
            </Card.Text>
            {cart.cartItems.find((item) => item.id === product.id) ? (
               <Link to="/cart">Manage in cart</Link>
            ) : product.countinstock > 0 ? (
               <Button onClick={() => addToCartHandler(product)}>
                  Add to cart
               </Button>
            ) : (
               <Button variant="light" disabled>
                  Out of stock
               </Button>
            )}
         </Card.Body>
      </Card>
   );
};

export default Product;
