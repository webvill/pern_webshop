import React, { useContext, useEffect, useReducer } from "react";
//import { PayPalButtons, usePayPalScriptReducer } from "@paypal/react-paypal-js";
//import { Helmet } from "react-helmet-async";
import { useNavigate, useParams } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import LoadingBox from "../components/LoadingBox";
import MessageBox from "../components/MessageBox";
import { Store } from "../Store";
import { getError } from "../utils";
import { toast } from "react-toastify";
import api from "../services/api";
import PayButton from "../components/PayButton";

function reducer(state, action) {
   switch (action.type) {
      case "FETCH_REQUEST":
         return { ...state, loading: true, error: "" };
      case "FETCH_SUCCESS":
         console.log("action payload", action.payload);
         return {
            ...state,
            loading: false,
            order: action.payload.order,
            orderItems: action.payload.items,
            error: "",
         };
      case "FETCH_FAIL":
         return { ...state, loading: false, error: action.payload };
      case "PAY_REQUEST":
         return { ...state, loadingPay: true };
      case "PAY_SUCCESS":
         return {
            ...state,
            loadingPay: false,
            successPay: true,
            order: { ...state.order, is_paid: true },
         };
      case "PAY_FAIL":
         return { ...state, loadingPay: false };
      case "PAY_RESET":
         return { ...state, loadingPay: false, successPay: false };

      case "DELIVER_REQUEST":
         return { ...state, loadingDeliver: true };
      case "DELIVER_SUCCESS":
         return {
            ...state,
            order: {
               ...state.order,
               is_delivered: true,
               delivered_at: action.payload,
            },
            loadingDeliver: false,
            successDeliver: true,
         };
      case "DELIVER_FAIL":
         return { ...state, loadingDeliver: false };
      case "DELIVER_RESET":
         return {
            ...state,
            loadingDeliver: false,
            successDeliver: false,
         };
      default:
         return state;
   }
}
export default function OrderScreen() {
   const { state } = useContext(Store);
   const {
      user: { userInfo },
   } = state;
   const params = useParams();
   const { id: orderId } = params;
   const navigate = useNavigate();

   const [
      {
         loading,
         error,
         order,
         orderItems,
         successPay,
         loadingPay,
         loadingDeliver,
         successDeliver,
      },
      dispatch,
   ] = useReducer(reducer, {
      loading: false,
      error: "",
      order: {},
      orderItems: [],
      successPay: false,
      loadingPay: false,
      loadingDeliver: false,
      successDeliver: true,
   });

   // const [{ isPending }, paypalDispatch] = usePayPalScriptReducer();

   function payReducer(state, action) {
      switch (action.type) {
         case "resetOptions":
            return { ...state, value: action.value };
         case "setLoadingStatus":
            return { ...state, value: action.value };
         default:
            return state;
      }
   }
   const [{ isPending }, payDispatch] = useReducer(payReducer, {
      isPending: false,
   });

   function createOrder(actions) {
      return actions.order
         .create({
            purchase_units: [
               {
                  amount: { value: order.total_price },
               },
            ],
         })
         .then((orderId) => {
            console.log("orderId", orderId);
            return orderId;
         });
   }

   function onApprove(actions) {
      return actions.order.capture().then(async (/* details */) => {
         console.log("in");
         try {
            dispatch({ type: "PAY_REQUEST" });
            const { data } = await api.put(
               `/orders/${order.id}/pay` /* details */,
               {
                  headers: { authorization: `Bearer ${userInfo.token}` },
               }
            );
            dispatch({ type: "PAY_SUCCESS", payload: data });
            toast.success("Order is paid");
         } catch (err) {
            dispatch({ type: "PAY_FAIL", payload: getError(err) });
            toast.error(getError(err));
         }
      });
   }
   function onError(err) {
      toast.error(getError(err));
   }

   useEffect(() => {
      const fetchOrder = async () => {
         try {
            dispatch({ type: "FETCH_REQUEST" });
            const { data } = await api.get(`/orders/${parseInt(orderId)}`, {
               headers: { authorization: `Bearer ${userInfo.token}` },
            });
            console.log("data fetch", data.data);
            dispatch({ type: "FETCH_SUCCESS", payload: data.data });
         } catch (err) {
            dispatch({ type: "FETCH_FAIL", payload: getError(err) });
         }
      };

      if (!userInfo) {
         return navigate("/login");
      }
      // check with reducer
      if (
         !order.id ||
         successPay ||
         successDeliver ||
         (parseInt(order.id) && order.id !== parseInt(orderId))
      ) {
         console.log("order.id", order.id);
         console.log("successPay", successPay);
         console.log("orderId", orderId);
         console.log("successDeliver", successDeliver);
         fetchOrder();

         if (successPay) {
            dispatch({ type: "PAY_RESET" });
         }
         if (successDeliver) {
            dispatch({ type: "DELIVER_RESET" });
         }
      } else {
         const loadPaypalScript = async () => {
            const { data: clientId } = await api.get("/api/keys/paypal", {
               headers: { authorization: `Bearer ${userInfo.token}` },
            });
            payDispatch({
               type: "resetOptions",
               value: {
                  "client-id": clientId,
                  currency: "USD",
               },
            });
            payDispatch({ type: "setLoadingStatus", value: "pending" });
         };
         loadPaypalScript();
      }
   }, []);

   async function deliverOrderHandler() {
      try {
         dispatch({ type: "DELIVER_REQUEST" });
         const { data } = await api.put(
            `/orders/${order.id}/deliver`,
            {},
            {
               headers: { authorization: `Bearer ${userInfo.token}` },
            }
         );
         console.log("DATA ", data)
         dispatch({ type: "DELIVER_SUCCESS", payload: data.delivered_at });
         toast.success("Order is delivered");
      } catch (err) {
         toast.error(getError(err));
         dispatch({ type: "DELIVER_FAIL" });
      }
   }

   return loading ? (
      <LoadingBox></LoadingBox>
   ) : error ? (
      <MessageBox variant="danger">{error}</MessageBox>
   ) : (
      <div>
         {/*  <Helmet>
            <title>Order {orderId}</title>
         </Helmet> */}
         <h1 className="my-3">Order {orderId}</h1>
         <Row>
            <Col md={8}>
               <Card className="mb-3">
                  <Card.Body>
                     <Card.Title>Shipping</Card.Title>
                     <Card.Text>
                        <strong>Name:</strong> {order.fullName} <br />
                        <strong>Address: </strong> {order.address},{order.city},{" "}
                        {order.postal_code},{order.country}
                        &nbsp;
                        {/* {order.shippingAddress.location &&
                           order.shippingAddress.location.lat && (
                              <a
                                 target="_new"
                                 href={`https://maps.google.com?q=${order.shippingAddress.location.lat},${order.shippingAddress.location.lng}`}
                              >
                                 Show On Map
                              </a>
                           )} */}
                     </Card.Text>
                     {order.is_delivered ? (
                        <MessageBox variant="success">
                           Delivered at {order.delivered_at}
                        </MessageBox>
                     ) : (
                        <MessageBox variant="danger">Not Delivered</MessageBox>
                     )}
                  </Card.Body>
               </Card>
               <Card className="mb-3">
                  <Card.Body>
                     <Card.Title>Payment</Card.Title>
                     <Card.Text>
                        <strong>Method:</strong> {order.payment_method}
                     </Card.Text>
                     {order.is_paid ? (
                        <MessageBox variant="success">
                           Paid at {order.paid_at}
                        </MessageBox>
                     ) : (
                        <MessageBox variant="danger">Not Paid</MessageBox>
                     )}
                  </Card.Body>
               </Card>

               <Card className="mb-3">
                  <Card.Body>
                     <Card.Title>Items</Card.Title>
                     <ListGroup variant="flush">
                        {orderItems.map((item) => (
                           <ListGroup.Item key={item.id}>
                              <Row className="align-items-center">
                                 <Col md={6}>
                                    <img
                                       src={item.image}
                                       alt={item.name}
                                       className="img-fluid rounded img-thumbnail"
                                    ></img>{" "}
                                    <Link to={`/product/${item.slug}`}>
                                       {item.name}
                                    </Link>
                                 </Col>
                                 <Col md={3}>
                                    <span>{item.quantity}</span>
                                 </Col>
                                 <Col md={3}>${item.price}</Col>
                              </Row>
                           </ListGroup.Item>
                        ))}
                     </ListGroup>
                  </Card.Body>
               </Card>
            </Col>
            <Col md={4}>
               <Card className="mb-3">
                  <Card.Body>
                     <Card.Title>Order Summary</Card.Title>
                     <ListGroup variant="flush">
                        <ListGroup.Item>
                           <Row>
                              <Col>Items</Col>
                              <Col>
                                 ${parseInt(order.items_price).toFixed(2)}
                              </Col>
                           </Row>
                        </ListGroup.Item>
                        <ListGroup.Item>
                           <Row>
                              <Col>Shipping</Col>
                              <Col>
                                 ${parseInt(order.shipping_price).toFixed(2)}
                              </Col>
                           </Row>
                        </ListGroup.Item>
                        <ListGroup.Item>
                           <Row>
                              <Col>Tax</Col>
                              <Col>${parseInt(order.tax_price).toFixed(2)}</Col>
                           </Row>
                        </ListGroup.Item>
                        <ListGroup.Item>
                           <Row>
                              <Col>
                                 <strong> Order Total</strong>
                              </Col>
                              <Col>
                                 <strong>
                                    ${parseInt(order.total_price).toFixed(2)}
                                 </strong>
                              </Col>
                           </Row>
                        </ListGroup.Item>
                        {!order.is_paid && (
                           <ListGroup.Item>
                              {isPending ? (
                                 <LoadingBox />
                              ) : (
                                 <div>
                                    <PayButton
                                       createOrder={createOrder}
                                       onApprove={onApprove}
                                       onError={onError}
                                    ></PayButton>
                                 </div>
                              )}
                              {loadingPay && <LoadingBox></LoadingBox>}
                           </ListGroup.Item>
                        )}
                        {userInfo.isadmin &&
                           order.is_paid &&
                           !order.is_delivered && (
                              <ListGroup.Item>
                                 {loadingDeliver && <LoadingBox></LoadingBox>}
                                 <div className="d-grid">
                                    <Button
                                       type="button"
                                       onClick={deliverOrderHandler}
                                    >
                                       Deliver Order
                                    </Button>
                                 </div>
                              </ListGroup.Item>
                           )}
                     </ListGroup>
                  </Card.Body>
               </Card>
            </Col>
         </Row>
      </div>
   );
}
