import { useEffect, useContext } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Product from "../components/Product";
import Loading from "../components/Loading";
import MessageBox from "../components/MessageBox";
import { Store } from "../Store";
import api from "../services/api";

const HomeScreen = () => {
   const {state, dispatch} = useContext(Store);
    
   const {products} = state;
   console.log("products", products)
   
    useEffect(() => {
       const fetchData = async () => {
          dispatch({ type: "FETCH_REQUEST" });
          
             await api.get("/products").then(({data}) => dispatch({ type: "FETCH_SUCCESS", payload: data.data }))
          .catch(error => 
             dispatch({ type: "FETCH_FAIL", payload: error.message }))
       };
       fetchData();
    }, []);
   return (
      <div>
         <h1>Featured Products</h1>
         <div className="products">
            {products.loading ? (
               <Loading />
            ) : products.error ? (
               <MessageBox variant="danger">{products.error}</MessageBox>
            ) : (
               <Row>
                  {products.products.map((product) => (
                     <Col
                        key={product.id}
                        sm={6}
                        md={4}
                        lg={3}
                        className="mb-3"
                     >
                        <Product product={product} />
                     </Col>
                  ))}
               </Row>
            )}
         </div>
      </div>
   );
};

export default HomeScreen;
