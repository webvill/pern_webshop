import React, { useContext, useEffect, useReducer } from "react";
import api from "../../services/api";
//import { Helmet } from "react-helmet-async";
import LoadingBox from "../../components/LoadingBox";
import MessageBox from "../../components/MessageBox";
import { Store } from "../../Store";
import { getError } from "../../utils";
import { Check2, PencilFill, Trash2Fill } from "react-bootstrap-icons";

const reducer = (state, action) => {
   switch (action.type) {
      case "FETCH_REQUEST":
         return { ...state, loading: true };
      case "FETCH_SUCCESS":
         return {
            ...state,
            users: action.payload,
            loading: false,
         };
      case "FETCH_FAIL":
         return { ...state, loading: false, error: action.payload };

      default:
         return state;
   }
};
export default function UserListScreen() {
   const [{ loading, error, users }, dispatch] = useReducer(reducer, {
      loading: true,
      error: "",
      users: [],
   });

   const { state } = useContext(Store);
   const {
      user: { userInfo },
   } = state;

   useEffect(() => {
      const fetchData = async () => {
         try {
            dispatch({ type: "FETCH_REQUEST" });
            const { data } = await api.get(`/users`, {
               headers: { Authorization: `Bearer ${userInfo.token}` },
            });
            console.log("users", data);
            dispatch({ type: "FETCH_SUCCESS", payload: data });
         } catch (err) {
            dispatch({
               type: "FETCH_FAIL",
               payload: getError(err),
            });
         }
      };
      fetchData();
   }, [userInfo]);
   return (
      <div>
         {/* <Helmet>
            <title>Users</title>
         </Helmet> */}
         <h1>Users</h1>
         {loading ? (
            <LoadingBox></LoadingBox>
         ) : error ? (
            <MessageBox variant="danger">{error}</MessageBox>
         ) : (
            <table className="table">
               <thead>
                  <tr>
                     <th>ID</th>
                     <th>NAME</th>
                     <th>EMAIL</th>
                     <th>IS ADMIN</th>
                     <th>ACTIONS</th>
                  </tr>
               </thead>
               <tbody>
                  {users.map((user) => (
                     <tr key={user.id}>
                        <td>{user.id}</td>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td>{user.isadmin && <Check2 color="green" />}</td>
                        <td>
                           <PencilFill
                              color="orange"
                              onClick={() => console.log("click")}
                           />
                           &nbsp;
                           <Trash2Fill
                              color="tomato"
                              onClick={() => console.log("click")}
                           />
                        </td>
                     </tr>
                  ))}
               </tbody>
            </table>
         )}
      </div>
   );
}
