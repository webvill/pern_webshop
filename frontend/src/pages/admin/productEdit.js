import React, { useContext, useEffect, useReducer, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { Store } from "../../Store";
import { getError } from "../../utils";
import Container from "react-bootstrap/Container";

import LoadingBox from "../../components/LoadingBox";
import MessageBox from "../../components/MessageBox";
import api from "../../services/api";
import ProductForm from "../../components/ProductForm";

const reducer = (state, action) => {
   switch (action.type) {
      case "FETCH_REQUEST":
         return { ...state, loading: true };
      case "FETCH_SUCCESS":
         return { ...state, loading: false };
      case "FETCH_FAIL":
         return { ...state, loading: false, error: action.payload };
      case "UPDATE_REQUEST":
         return { ...state, loadingUpdate: true };
      case "UPDATE_SUCCESS":
         return { ...state, loadingUpdate: false };
      case "UPDATE_FAIL":
         return { ...state, loadingUpdate: false };

      default:
         return state;
   }
};
export default function ProductEditScreen() {
   const navigate = useNavigate();
   const params = useParams(); // /product/:id
   const { id: productId } = params;
   const [product, setProduct] = useState({});

   const { state } = useContext(Store);
   const {
      user: { userInfo },
   } = state;
   const [{ loading, error }, dispatch] = useReducer(reducer, {
      loading: true,
      error: "",
   });

   useEffect(() => {
      const fetchData = async () => {
         try {
            dispatch({ type: "FETCH_REQUEST" });
            const {
               data: { data },
            } = await api.get(`/products/${productId}`);

            let product = data.product;
            product.images = data.images;
            setProduct(product);

            dispatch({ type: "FETCH_SUCCESS" });
         } catch (err) {
            dispatch({
               type: "FETCH_FAIL",
               payload: getError(err),
            });
         }
      };
      fetchData();
   }, [productId]);

   const submitHandler = async (e, data) => {
      console.log("product", data);
      e.preventDefault();
      try {
         dispatch({ type: "UPDATE_REQUEST" });
         await api.put(
            `/products/${productId}`,
            {
               name: data.name,
               slug: data.slug,
               price: data.price,
               image: data.image,
               images: data.images,
               category_id: data.category,
               brand_id: data.brand,
               countinstock: data.countInStock,
               description: data.description,
            },
            {
               headers: { Authorization: `Bearer ${userInfo.token}` },
            }
         );
         dispatch({
            type: "UPDATE_SUCCESS",
         });
         toast.success("Product updated successfully");
         navigate("/admin/products");
      } catch (err) {
         toast.error(getError(err));
         dispatch({ type: "UPDATE_FAIL" });
      }
   };

   return (
      <Container className="small-container">
         {/* <Helmet>
            <title>Edit Product ${productId}</title>
         </Helmet> */}
         <h1>Edit Product {productId}</h1>

         {loading ? (
            <LoadingBox></LoadingBox>
         ) : error ? (
            <MessageBox variant="danger">{error}</MessageBox>
         ) : (
            <ProductForm
               submitHandler={submitHandler}
               user={userInfo}
               product={product}
            />
         )}
      </Container>
   );
}
