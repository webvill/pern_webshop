import React, { useReducer, useContext } from "react";
import { useNavigate } from "react-router-dom";
import Container from "react-bootstrap/Container";
import { toast } from "react-toastify";
import ProductForm from "../../components/ProductForm";
import api from "../../services/api";
import { Store } from "../../Store";
import { getError } from "../../utils";
import LoadingBox from "../../components/LoadingBox";
import MessageBox from "../../components/MessageBox";

const reducer = (state, action) => {
   switch (action.type) {
      case "POST_REQUEST":
         return { ...state, loading: true };
      case "POST_SUCCESS":
         return { ...state, loading: false };
      case "POST_FAIL":
         return { ...state, loading: false };
      default:
         return state;
   }
};
export default function ProductCreateScreen() {
   const [{ loading, error }, dispatch] = useReducer(reducer, {
      loading: false,
      error: "",
   });

   const navigate = useNavigate();
   const { state } = useContext(Store);
   const {
      user: { userInfo },
   } = state;

   const submitHandler = async (e, data) => {
      console.log("product", data);
      e.preventDefault();
      try {
         dispatch({ type: "POST_REQUEST" });
         await api.post(
            "/products",
            {
               name: data.name,
               slug: data.slug,
               price: data.price,
               image: data.image,
               images: data.images,
               category_id: data.category,
               brand_id: data.brand,
               countinstock: data.countInStock,
               description: data.description,
            },
            {
               headers: { Authorization: `Bearer ${userInfo.token}` },
            }
         );
         dispatch({
            type: "POST_SUCCESS",
         });
         toast.success("Product created successfully");
         navigate("/admin/products");
      } catch (err) {
         toast.error(getError(err));
         dispatch({ type: "POST_FAIL" });
      }
   };

   return (
      <Container className="small-container">
         <h1>Add new product</h1>
         {loading ? (
            <LoadingBox></LoadingBox>
         ) : error ? (
            <MessageBox variant="danger">{error}</MessageBox>
         ) : (
            <ProductForm submitHandler={submitHandler} user={userInfo} />
         )}
      </Container>
   );
}
