import React, { useContext, useEffect, useReducer } from "react";
import {
   PieChart,
   Pie,
   Cell,
   LineChart,
   Line,
   XAxis,
   YAxis,
   CartesianGrid,
   Tooltip,
   Legend,
} from "recharts";
import { Store } from "../../Store";
import { getError } from "../../utils";
import LoadingBox from "../../components/LoadingBox";
import MessageBox from "../../components/MessageBox";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import api from "../../services/api";

const reducer = (state, action) => {
   switch (action.type) {
      case "FETCH_REQUEST":
         return { ...state, loading: true };
      case "FETCH_SUCCESS":
         return {
            ...state,
            summary: action.payload,
            loading: false,
         };
      case "FETCH_FAIL":
         return { ...state, loading: false, error: action.payload };
      default:
         return state;
   }
};
const lineData = [
   {
      name: "Page A",
      uv: 4000,
      pv: 2400,
      amt: 2400,
   },
   {
      name: "Page B",
      uv: 3000,
      pv: 1398,
      amt: 2210,
   },
   {
      name: "Page C",
      uv: 2000,
      pv: 9800,
      amt: 2290,
   },
   {
      name: "Page D",
      uv: 2780,
      pv: 3908,
      amt: 2000,
   },
   {
      name: "Page E",
      uv: 1890,
      pv: 4800,
      amt: 2181,
   },
   {
      name: "Page F",
      uv: 2390,
      pv: 3800,
      amt: 2500,
   },
   {
      name: "Page G",
      uv: 3490,
      pv: 4300,
      amt: 2100,
   },
];
const data = [
   { name: "Group A", value: 400 },
   { name: "Group B", value: 300 },
   { name: "Group C", value: 300 },
   { name: "Group D", value: 200 },
];

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
   cx,
   cy,
   midAngle,
   innerRadius,
   outerRadius,
   percent,
   index,
}) => {
   const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
   const x = cx + radius * Math.cos(-midAngle * RADIAN);
   const y = cy + radius * Math.sin(-midAngle * RADIAN);

   return (
      <text
         x={x}
         y={y}
         fill="white"
         textAnchor={x > cx ? "start" : "end"}
         dominantBaseline="central"
      >
         {`${(percent * 100).toFixed(0)}%`}
      </text>
   );
};
export default function DashboardScreen() {
   const [{ loading, summary, error }, dispatch] = useReducer(reducer, {
      loading: true,
      error: "",
   });
   const { state } = useContext(Store);
   const {
      user: { userInfo },
   } = state;

   useEffect(() => {
      const fetchData = async () => {
         try {
            const { data } = await api.get("/orders/summary", {
               headers: { Authorization: `Bearer ${userInfo.token}` },
            });
            dispatch({ type: "FETCH_SUCCESS", payload: data.data });
         } catch (err) {
            dispatch({
               type: "FETCH_FAIL",
               payload: getError(err),
            });
         }
      };
      fetchData();
   }, [userInfo]);

   return (
      <div>
         <h3 className="fw-bold opacity-50">Dashboard</h3>
         {loading ? (
            <LoadingBox />
         ) : error ? (
            <MessageBox variant="danger">{error}</MessageBox>
         ) : (
            <>
               <Row>
                  <Col md={4}>
                     <Card>
                        <Card.Body>
                           <Card.Title>
                              {summary.users && summary.users[0]
                                 ? summary.users[0].numUsers
                                 : 0}
                           </Card.Title>
                           <Card.Text> Users</Card.Text>
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={4}>
                     <Card>
                        <Card.Body>
                           <Card.Title>
                              {summary.orders && summary.users[0]
                                 ? summary.orders[0].numOrders
                                 : 0}
                           </Card.Title>
                           <Card.Text> Orders</Card.Text>
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={4}>
                     <Card>
                        <Card.Body>
                           <Card.Title>
                              $
                              {summary.orders && summary.users[0]
                                 ? summary.orders[0].totalSales.toFixed(2)
                                 : 0}
                           </Card.Title>
                           <Card.Text> Orders</Card.Text>
                        </Card.Body>
                     </Card>
                  </Col>
               </Row>
               <div className="my-3">
                  <h3 className="fw-light opacity-50">Sales</h3>
                  {summary.dailyOrders.length === 0 ? (
                     <MessageBox>No Sale</MessageBox>
                  ) : (
                     <LineChart
                        width={500}
                        height={300}
                        data={lineData}
                        margin={{
                           top: 5,
                           right: 30,
                           left: 20,
                           bottom: 5,
                        }}
                     >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line
                           type="monotone"
                           dataKey="pv"
                           stroke="#8884d8"
                           activeDot={{ r: 8 }}
                        />
                        <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
                     </LineChart>
                  )}
               </div>
               <div className="my-3">
                  <h3 className="fw-light opacity-50">Categories</h3>
                  {summary.productCategories.length === 0 ? (
                     <MessageBox>No Category</MessageBox>
                  ) : (
                     <>
                        <PieChart width={400} height={400}>
                           <Pie
                              data={data}
                              cx="50%"
                              cy="50%"
                              labelLine={false}
                              label={renderCustomizedLabel}
                              outerRadius={80}
                              fill="#8884d8"
                              dataKey="value"
                           >
                              {data.map((entry, index) => (
                                 <Cell
                                    key={`cell-${index}`}
                                    fill={COLORS[index % COLORS.length]}
                                 />
                              ))}
                           </Pie>
                        </PieChart>
                     </>
                  )}
               </div>
            </>
         )}
      </div>
   );
}
