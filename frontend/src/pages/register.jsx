import { useState } from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import api from "../services/api";
const RegisterScreen = () => {
   const [name, setName] = useState("");
   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");

   const submitHandler = async (e) => {
      e.preventDefault();

      await api
         .post("/users/register", {
            name: name,
            email: email,
            password: password,
         })
         .then((resp) => console.log(resp))
         .catch((error) => console.log(error));
   };
   return (
      <Container className="small-container">
         <title>Sign In</title>
         <h1 className="my-3">Register</h1>
         <Form onSubmit={submitHandler}>
            <Form.Group className="mb-3" controlId="name">
               <Form.Label>Name</Form.Label>
               <Form.Control
                  type="name"
                  required
                  onChange={(e) => setName(e.target.value)}
               />
            </Form.Group>
            <Form.Group className="mb-3" controlId="email">
               <Form.Label>Email</Form.Label>
               <Form.Control
                  type="email"
                  required
                  onChange={(e) => setEmail(e.target.value)}
               />
            </Form.Group>
            <Form.Group className="mb-3" controlId="password">
               <Form.Label>Password</Form.Label>
               <Form.Control
                  type="password"
                  required
                  onChange={(e) => setPassword(e.target.value)}
               />
            </Form.Group>
            <div className="mb-3">
               <Button type="submit">Register</Button>
            </div>
         </Form>
      </Container>
   );
};

export default RegisterScreen;
