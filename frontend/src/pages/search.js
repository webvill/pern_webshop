import React, { useEffect, useReducer, useState } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import { getError } from "../utils";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Rating from "../components/Rating";
import LoadingBox from "../components/LoadingBox";
import MessageBox from "../components/MessageBox";
import Button from "react-bootstrap/Button";
import Product from "../components/Product";
import LinkContainer from "react-router-bootstrap/LinkContainer";
import api from "../services/api";
import Card from "react-bootstrap/Card";
import { XCircleFill } from "react-bootstrap-icons";

const reducer = (state, action) => {
   switch (action.type) {
      case "FETCH_REQUEST":
         return { ...state, loading: true };
      case "FETCH_SUCCESS":
         return {
            ...state,
            products: action.payload.products,
            page: action.payload.page,
            pages: action.payload.pages,
            countProducts: action.payload.countProducts,
            loading: false,
         };
      case "FETCH_FAIL":
         return { ...state, loading: false, error: action.payload };

      default:
         return state;
   }
};

const prices = [
   {
      name: "$1 to $50",
      value: "1-50",
   },
   {
      name: "$51 to $200",
      value: "51-200",
   },
   {
      name: "$201 to $1000",
      value: "201-1000",
   },
];

export const ratings = [
   {
      name: "4stars & up",
      rating: 4,
   },

   {
      name: "3stars & up",
      rating: 3,
   },

   {
      name: "2stars & up",
      rating: 2,
   },

   {
      name: "1stars & up",
      rating: 1,
   },
];

export default function SearchScreen({ setResetSearchForm }) {
   const navigate = useNavigate();
   const { search } = useLocation();
   const sp = new URLSearchParams(search); // /search?category=Shirts
   const category = sp.get("category") || "all";
   const brand = sp.get("brand") || "all";
   const query = sp.get("query") || "all";
   const price = sp.get("price") || "all";
   const rating = sp.get("rating") || "all";
   const order = sp.get("order") || "newest";
   const page = sp.get("page") || 1;

   const [categories, setCategories] = useState([]);
   const brands = ["Adidas", "Nike"];
   const [filters, setFilters] = useState({
      category: category !== "all" ? 1 : 0,
      brand: brand !== "all" ? 1 : 0,
      query: query !== "all" ? 1 : 0,
      rating: rating !== "all" ? 1 : 0,
   });

   const [{ loading, error, products, pages, countProducts }, dispatch] =
      useReducer(reducer, {
         loading: true,
         error: "",
      });

   useEffect(() => {
      const fetchData = async () => {
         await api
            .get(
               `/products/search?page=${page}&query=${query}&category=${category}&brand=${brand}&price=${price}&rating=${rating}&order=${order} 
               `
            )
            .then(({ data }) =>
               dispatch({ type: "FETCH_SUCCESS", payload: data })
            )
            .catch((err) =>
               dispatch({ type: "FETCH_FAIL", payload: getError(error) })
            );
      };
      fetchData();
   }, [category, brand, error, order, page, price, query, rating]);

   useEffect(() => {
      const fetchCategories = async () => {
         try {
            const { data } = await api.get(`/products/categories`);
            setCategories(data);
         } catch (err) {
            toast.error(getError(err));
         }
      };
      fetchCategories();
   }, [dispatch]);

   //
   const getFilterUrl = (filter) => {
      const filterPage = filter.page || page;
      const filterCategory = filter.category || category;
      const filterBrand = filter.brand || brand;
      const filterQuery = filter.query || query;
      const filterRating = filter.rating || rating;
      const filterPrice = filter.price || price;
      const sortOrder = filter.order || order;
      return `/search?category=${filterCategory}&brand=${filterBrand}&query=${filterQuery}&price=${filterPrice}&rating=${filterRating}&order=${sortOrder}&page=${filterPage}`;
   };
   return (
      <div>
         {/* <Helmet>
            <title>Search Products</title>
         </Helmet> */}
         <Row>
            <Col md={3}>
               <Card className="card shadow mb-3">
                  <span className="px-3 fw-bold fs-5 text-dark opacity-50">
                     Department
                  </span>
                  <Card.Body>
                     <ul className="list-group" style={{ listStyle: "none" }}>
                        {categories.map((c) => (
                           <li key={c.id}>
                              <Link
                                 onClick={() =>
                                    setFilters({ ...filters, category: 1 })
                                 }
                                 style={{ textDecoration: "none" }}
                                 className={
                                    c.name === category ? "fw-bold" : ""
                                 }
                                 to={getFilterUrl({ category: c.name })}
                              >
                                 {c.name}
                              </Link>
                           </li>
                        ))}
                     </ul>
                  </Card.Body>
               </Card>
               <Card className="card shadow my-3">
                  <span className="px-3 fw-bold fs-5 text-dark opacity-50">
                     Brand
                  </span>
                  <Card.Body>
                     <ul className="list-group" style={{ listStyle: "none" }}>
                        {brands.map((b) => (
                           <li key={b}>
                              <Link
                                 onClick={() =>
                                    setFilters({ ...filters, brand: 1 })
                                 }
                                 style={{ textDecoration: "none" }}
                                 className={b === brand ? "fw-bold" : ""}
                                 to={getFilterUrl({ brand: b })}
                              >
                                 {b}
                              </Link>
                           </li>
                        ))}
                     </ul>
                  </Card.Body>
               </Card>
               <Card className="card shadow my-3">
                  <span className="px-3 fw-bold fs-5 text-dark opacity-50">
                     Price
                  </span>
                  <Card.Body>
                     <ul className="list-group" style={{ listStyle: "none" }}>
                        {prices.map((p) => (
                           <li key={p.value}>
                              <Link
                                 onClick={() =>
                                    setFilters({ ...filters, price: 1 })
                                 }
                                 style={{ textDecoration: "none" }}
                                 to={getFilterUrl({ price: p.value })}
                                 className={
                                    p.value === price
                                       ? "fw-bold text-dark opacity-50"
                                       : "fw-light text-dark opacity-75"
                                 }
                              >
                                 {p.name}
                              </Link>
                           </li>
                        ))}
                     </ul>
                  </Card.Body>
               </Card>
               <Card className="card shadow my-3">
                  <span className="px-3 fw-bold fs-5 text-dark opacity-50">
                     Customer rating
                  </span>
                  <Card.Body>
                     <ul className="list-group" style={{ listStyle: "none" }}>
                        {ratings.map((r) => (
                           <li key={r.name}>
                              <Link
                                 onClick={() =>
                                    setFilters({ ...filters, rating: 1 })
                                 }
                                 to={getFilterUrl({ rating: r.rating })}
                                 className={
                                    `${r.rating}` === `${rating}`
                                       ? "fw-bold"
                                       : ""
                                 }
                              >
                                 <Rating
                                    caption={" & up"}
                                    rating={r.rating}
                                 ></Rating>
                              </Link>
                           </li>
                        ))}
                        <li>
                           <Link
                              to={getFilterUrl({ rating: "all" })}
                              className={rating === "all" ? "fw-bold" : ""}
                           >
                              <Rating caption={" & up"} rating={0}></Rating>
                           </Link>
                        </li>
                     </ul>
                  </Card.Body>
               </Card>
            </Col>
            <Col md={9}>
               {loading ? (
                  <LoadingBox></LoadingBox>
               ) : error ? (
                  <MessageBox variant="danger">{error}</MessageBox>
               ) : (
                  <>
                     <Row className="justify-content-between mb-3">
                        <Col md={6}>
                           <div>
                              {query !== "all" && (
                                 <span
                                    className="badge bg-primary me-1"
                                    onClick={() => {
                                       setFilters({ ...filters, query: 0 });
                                       setResetSearchForm(true);
                                    }}
                                 >
                                    {query}&nbsp;{" "}
                                    <XCircleFill
                                       style={{ cursor: "pointer" }}
                                       onClick={() =>
                                          navigate(
                                             getFilterUrl({
                                                query: "all",
                                             })
                                          )
                                       }
                                    />
                                 </span>
                              )}
                              {category !== "all" && (
                                 <span
                                    className="badge bg-primary me-1"
                                    onClick={() =>
                                       setFilters({ ...filters, category: 0 })
                                    }
                                 >
                                    {category}&nbsp;{" "}
                                    <XCircleFill
                                       style={{ cursor: "pointer" }}
                                       onClick={() =>
                                          navigate(
                                             getFilterUrl({
                                                category: "all",
                                             })
                                          )
                                       }
                                    />
                                 </span>
                              )}
                              {brand !== "all" && (
                                 <span
                                    className="badge bg-primary me-1"
                                    onClick={() =>
                                       setFilters({ ...filters, brand: 0 })
                                    }
                                 >
                                    {brand}&nbsp;{" "}
                                    <XCircleFill
                                       style={{ cursor: "pointer" }}
                                       onClick={() =>
                                          navigate(
                                             getFilterUrl({ brand: "all" })
                                          )
                                       }
                                    />
                                 </span>
                              )}
                              {price !== "all" && (
                                 <span
                                    className="badge bg-primary me-1"
                                    onClick={() =>
                                       setFilters({ ...filters, price: 0 })
                                    }
                                 >
                                    {price}&nbsp;{" "}
                                    <XCircleFill
                                       style={{ cursor: "pointer" }}
                                       onClick={() =>
                                          navigate(
                                             getFilterUrl({ price: "all" })
                                          )
                                       }
                                    />
                                 </span>
                              )}
                              {rating !== "all" && (
                                 <span
                                    className="badge bg-primary me-1"
                                    onClick={() =>
                                       setFilters({ ...filters, rating: 0 })
                                    }
                                 >
                                    {rating}&nbsp; and up{" "}
                                    <XCircleFill
                                       style={{ cursor: "pointer" }}
                                       onClick={() =>
                                          navigate(
                                             getFilterUrl({ rating: "all" })
                                          )
                                       }
                                    />
                                 </span>
                              )}
                              {Object.values(filters).reduce((a, b) => a + b) >
                                 1 && (
                                 <XCircleFill
                                    color="tomato"
                                    onClick={() => navigate("/search")}
                                    style={{ cursor: "pointer" }}
                                 />
                              )}
                           </div>
                        </Col>
                        <Col className="text-center">
                           {countProducts !== 0 && countProducts + " results"}
                        </Col>
                        <Col className="text-end">
                           <select
                              className="form-select form-select-sm"
                              value={order}
                              onChange={(e) => {
                                 navigate(
                                    getFilterUrl({ order: e.target.value })
                                 );
                              }}
                           >
                              <option value="newest">Nyaste</option>
                              <option value="lowest">
                                 Pris: Lågt till högt
                              </option>
                              <option value="highest">
                                 Pris: Högt till lågt
                              </option>
                              <option value="toprated">Användarbetyg</option>
                           </select>
                        </Col>
                     </Row>
                     {products.length === 0 && (
                        <MessageBox>No Product Found</MessageBox>
                     )}

                     <Row>
                        {products.map((product) => (
                           <Col key={product.id} sm={6} lg={4} className="mb-3">
                              <Product product={product}></Product>
                           </Col>
                        ))}
                     </Row>

                     <div>
                        {products.length > 0 &&
                           [...Array(pages).keys()].map((x) => (
                              <LinkContainer
                                 key={x + 1}
                                 className="mx-1"
                                 to={getFilterUrl({ page: x + 1 })}
                              >
                                 <Button
                                    className={
                                       Number(page) === x + 1 ? "text-bold" : ""
                                    }
                                    variant="light"
                                 >
                                    {x + 1}
                                 </Button>
                              </LinkContainer>
                           ))}
                     </div>
                  </>
               )}
            </Col>
         </Row>
      </div>
   );
}
