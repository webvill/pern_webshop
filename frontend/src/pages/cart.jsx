import React from "react";
import { useContext } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Link, useNavigate } from "react-router-dom";
import MessageBox from "../components/MessageBox";
import { Store } from "../Store";
import { Dash, Plus, Trash2 } from "react-bootstrap-icons";
import api from "../services/api";

const CartScreen = () => {
   const navigate = useNavigate();
   const { state, dispatch } = useContext(Store);
   const {
      cart: { cartItems },
   } = state;
   console.log("cartItems", cartItems);

   const updateCartHandler = async (item, quantity) => {
      const { data } = await api.get(`/products/${item.id}`);
      console.log("data", data);
      if (data.data.product.countinstock <= quantity) {
         window.alert(`Sorry this is the last in stock`);
      }
      dispatch({
         type: "CART_ADD_ITEM",
         payload: { ...item, quantity },
      });
   };
   const removeItemHandler = (item) => {
      dispatch({ type: "CART_REMOVE_ITEM", payload: item });
   };
   return (
      <div>
         <h1>Your Cart</h1>
         <Row>
            <Col md={8}>
               {cartItems.length === 0 ? (
                  <MessageBox>
                     <Link to="/">Go shopping</Link>
                  </MessageBox>
               ) : (
                  <ListGroup>
                     {cartItems.map((item) => (
                        <ListGroup.Item key={item.id}>
                           <Row className="align-items-center">
                              <Col md={4}>
                                 <img
                                    src={item.image}
                                    alt={item.name}
                                    className="img-fluid img-thumbnail"
                                 />
                                 <Link to={`/products/slug/${item.slug}`}>
                                    {item.name}
                                 </Link>
                              </Col>
                              <Col md={3}>
                                 <Button
                                    className="me-3"
                                    onClick={() =>
                                       updateCartHandler(
                                          item,
                                          item.quantity - 1
                                       )
                                    }
                                    variant="light"
                                    size="sm"
                                    disabled={item.quantity === 1}
                                 >
                                    <Dash />
                                 </Button>
                                 {item.quantity}
                                 <Button
                                    className="ms-3"
                                    onClick={() =>
                                       updateCartHandler(
                                          item,
                                          item.quantity + 1
                                       )
                                    }
                                    variant="light"
                                    size="sm"
                                    disabled={
                                       item.quantity === item.countinstock
                                    }
                                 >
                                    <Plus />
                                 </Button>
                              </Col>
                              <Col md={2}>${item.price}</Col>
                              <Col md={1}>${item.quantity* item.price}</Col>
                              <Col md={1}>
                                 <Trash2
                                    onClick={() => removeItemHandler(item)}
                                    className="clickable"
                                    color="tomato"
                                 />
                              </Col>
                           </Row>
                        </ListGroup.Item>
                     ))}
                  </ListGroup>
               )}
            </Col>
            <Col md={4}>
               <Card>
                  <Card.Body variant="flush">
                     <ListGroup>
                        <ListGroup.Item>
                           <h3>
                              Subtotal (
                              {cartItems.reduce((a, c) => a + c.quantity, 0)}{" "}
                              items) : $
                              {cartItems.reduce(
                                 (a, c) => a + c.price * c.quantity,
                                 0
                              )}
                           </h3>
                        </ListGroup.Item>
                        <ListGroup.Item>
                           <div className="d-grid">
                              <Button
                                 onClick={() =>
                                    navigate("/signin?redirect=/shipping")
                                 }
                                 type="button"
                                 variant="primary"
                                 disable={
                                    cartItems.length === 0
                                       ? "disabled"
                                       : "undefined"
                                 }
                              >
                                 Checkhout
                              </Button>
                           </div>
                        </ListGroup.Item>
                     </ListGroup>
                  </Card.Body>
               </Card>
            </Col>
         </Row>
      </div>
   );
};

export default CartScreen;
