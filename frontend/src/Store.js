// store with useReducer and createContext
import { useMemo } from "react";
import { useReducer } from "react";
import { createContext } from "react";
export const Store = createContext();
const initialState = {
   user: {
      userInfo: localStorage.getItem("userInfo")
         ? JSON.parse(localStorage.getItem("userInfo"))
         : null,
      isAuth: false,
   },
   cart: {
      shippingAddress: localStorage.getItem("shippingAddress")
         ? JSON.parse(localStorage.getItem("shippingAddress"))
         : {},
      cartItems: localStorage.getItem("cartItems")
         ? JSON.parse(localStorage.getItem("cartItems"))
         : [],
      paymentMethod: localStorage.getItem("paymentMethod")
         ? localStorage.getItem("paymentMethod")
         : "",
   },
   products: { loading: true, error: "", products: [] },
   order: {loading: false}
};
function reducer(state, action) {
   switch (action.type) {
      case "USER_LOGIN":
         return { ...state, user: { userInfo: action.payload, isAuth: true } };
      case "USER_LOGOUT":
         return {
            ...state,
            user: { userInfo: null, isAuth: false },
            cart: { cartItems: [], shippingAddress: {}, paymentMethod: "" },
         };

      case "FETCH_SUCCESS":
         return {
            ...state,
            products: {
               ...state.products,
               products: action.payload,
               loading: false,
            },
         };

      case "FETCH_FAIL":
         return {
            ...state,
            products: {
               ...state.products,
               loading: false,
               error: action.payload,
            },
         };

      case "CART_ADD_ITEM":
         const newItem = action.payload;
         const existItem = state.cart.cartItems.find(
            (item) => item.id === newItem.id
         );
         const cartItems = existItem
            ? state.cart.cartItems.map((item) =>
                 item.id === existItem.id ? newItem : item
              )
            : [...state.cart.cartItems, newItem];
         localStorage.setItem("cartItems", JSON.stringify(cartItems));
         return {
            ...state,
            cart: {
               ...state.cart,
               cartItems,
            },
         };
      case "CART_REMOVE_ITEM": {
         const cartItems = state.cart.cartItems.filter(
            (item) => item.id !== action.payload.id
         );
         localStorage.setItem("cartItems", JSON.stringify(cartItems));
         return { ...state, cart: { ...state.cart, cartItems } };
      }
      case "CART_CLEAR": {
         return {...state, cart: {...state.cart, cartItems: []}}
      }
      case "SAVE_SHIPPING_ADDRES":
         return {
            ...state,
            cart: { ...state.cart, shippingAddress: action.payload },
         };
      case "SAVE_PAYMENT_METHOD":
         return {
            ...state,
            cart: { ...state.cart, paymentMethod: action.payload },
         };
         case "CREATE_ORDER":
         return { ...state, order:{loading: true} };
         case "ORDER_SUCCESS":
         return { ...state, order: { loading: false } };
         case "ORDER_FAIL":
         return { ...state, order: { loading: false } };
      
         default:
         return state;
   }
}
// HOC
export function StoreProvider(props) {
   const [state, dispatch] = useReducer(reducer, initialState);
   const value = useMemo(() => {
      return { state, dispatch };
   }, [state, dispatch]);
   return <Store.Provider value={value}>{props.children}</Store.Provider>;
}
